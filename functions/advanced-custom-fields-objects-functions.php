<?php

/**
 * This is a wrapper for the Wordpress get_posts function which will return an array of ACFPost objects instead of an array of WP_Post objects.
 * 
 * @param ambiguous <multitype|null> $args An associative array of args to pass to get_posts() or NULL.
 * @return multitype:ACFPost An array of the found WP_Post objects, wrapped in ACFPost objects.
 */
function acf_get_posts( $args = NULL ) {

	$posts = get_posts( $args );
	$_return = array();
	if( $posts && is_array($posts) ) {
		foreach( $posts as &$post ) {
			$_return[] = new ACFPost($post);
		}
	}
	return $_return;
}

/**
 * This is a wrapper for the Wordpress get_post function which will return an ACFPost object instead of a WP_Post object.
 * 
 * @param ambiguous <int|WP_Post> $id Post ID or WP_Post object.
 * @return ambiguous <ACFPost|null> The ACFPost containing the specified WP_Post object, or null on failure.
 */
function acf_get_post( $post ) {
	$post = get_post( $post );
	if( $post === null ) return null;
	return new ACFPost($post);
}

/**
 * This is a wrapper for the Wordpress get_term function which will return an ACFTerm object instead of a Wordpress term database row object.
 *
 * @param int|object $term If integer, will get from database. If object will apply filters and return $term.
 * @param string $taxonomy Taxonomy name that $term is part of.
 * @param string $output Constant OBJECT, ARRAY_A, or ARRAY_N
 * @param string $filter Optional, default is raw or no WordPress defined filter will applied.
 * @return ACFTerm|null|WP_Error ACFTerm object. Will return null if $term is empty. If taxonomy does not
 * exist then WP_Error will be returned.
 */

function acf_get_term($term, $taxonomy, $output = OBJECT, $filter = 'raw') {
	
	$term = get_term( $term , $taxonomy , $output , $filter );
	
	if( $term instanceof WP_Error ) return $term;
	if( $term === null ) return null;
	
	return new ACFTerm( $term );
	
}

/**
 * This is a wrapper for the Wordpress get_term_by function which will return an ACFTerm object instead of a Wordpress term database row object.
 *
 * @param string $field Either 'slug', 'name', 'id' (term_id), or 'term_taxonomy_id'
 * @param string|int $value Search for this term value
 * @param string $taxonomy Taxonomy Name
 * @param string $output Constant OBJECT, ARRAY_A, or ARRAY_N
 * @param string $filter Optional, default is raw or no WordPress defined filter will applied.
 * @return ACFTerm|boolean ACFTerm object. Will return false if $taxonomy does not exist or $term was not found.
 */
function acf_get_term_by($field, $value, $taxonomy, $output = OBJECT, $filter = 'raw') {
	
	$term = get_term_by( $field , $value , $taxonomy , $output , $filter );
	
	if( $term === false ) return false;
	
	return new ACFTerm( $term );
	
}

/**
 * This is a wrapper for the Wordpress get_terms function which will return an array of ACFTerm objects instead of an array of Wordpress term database row objects.
 *
 * @param string|array $taxonomies Taxonomy name or list of Taxonomy names.
 * @param array|string $args {
 *     Optional. Array or string of arguments to get terms.
 *
 *     @type string   $orderby               Field(s) to order terms by. Accepts term fields, though
 *                                           empty defaults to 'term_id'. Default 'name'.
 *     @type string   $order                 Whether to order terms in ascending or descending order.
 *                                           Accepts 'ASC' (ascending) or 'DESC' (descending).
 *                                           Default 'ASC'.
 *     @type bool|int     $hide_empty        Whether to hide terms not assigned to any posts. Accepts
 *                                           1|true or 0|false. Default 1|true.
 *     @type array|string $include           Array or comma/space-separated string of term ids to include.
 *                                           Default empty array.
 *     @type array|string $exclude           Array or comma/space-separated string of term ids to exclude.
 *                                           If $include is non-empty, $exclude is ignored.
 *                                           Default empty array.
 *     @type array|string $exclude_tree      Array or comma/space-separated string of term ids to exclude
 *                                           along with all of their descendant terms. If $include is
 *                                           non-empty, $exclude_tree is ignored. Default empty array.
 *     @type int          $number            Maximum number of terms to return. Accepts 1+ or -1 (all).
 *                                           Default -1.
 *     @type int          $offset            The number by which to offset the terms query. Default empty.
 *     @type string       $fields            Term fields to query for. Accepts 'all' (returns an array of
 *                                           term objects), 'ids' or 'names' (returns an array of integers
 *                                           or strings, respectively. Default 'all'.
 *     @type string       $slug              Slug to return term(s) for. Default empty.
 *     @type bool         $hierarchical      Whether to include terms that have non-empty descendants (even
 *                                           if $hide_empty is set to true). Default true.
 *     @type string       $search            Search criteria to match terms. Will be SQL-formatted with
 *                                           wildcards before and after. Default empty.
 *     @type string       $name__like        Retrieve terms with criteria by which a term is LIKE $name__like.
 *                                           Default empty.
 *     @type string       $description__like Retrieve terms where the description is LIKE $description__like.
 *                                           Default empty.
 *     @type bool         $pad_counts        Whether to pad the quantity of a term's children in the quantity
 *                                           of each term's "count" object variable. Default false.
 *     @type string       $get               Whether to return terms regardless of ancestry or whether the terms
 *                                           are empty. Accepts 'all' or empty (disabled). Default empty.
 *     @type int          $child_of          Term ID to retrieve child terms of. If multiple taxonomies
 *                                           are passed, $child_of is ignored. Default 0.
 *     @type int|string   $parent            Parent term ID to retrieve direct-child terms of. Default empty.
 *     @type string       $cache_domain      Unique cache key to be produced when this query is stored in an
 *                                           object cache. Default is 'core'.
 * }
 * @return array|WP_Error List of Term Objects and their children. Will return WP_Error, if any of $taxonomies
 *                        do not exist.
 */
function acf_get_terms( $taxonomies, $args = '' ) {
	
	$terms = get_terms( $taxonomies , $args );
	
	if( $terms instanceof WP_Error ) return $terms;
	
	foreach( $terms as &$term ) {
		$term = new ACFTerm($term);
	}
	
	return $terms;
	
}