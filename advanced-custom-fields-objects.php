<?php
/*
 Plugin Name: Advanced Custom Fields Objects
 Description: A set of objects and functions to help make working with the Advanced Custom Fields plugin a little easier for programmers.
 Version: 1.0.0
 Author: Brian Neff
 Author URI: http://vius.co/
 License: GPL
 */


define( 'ACFOBJECTS_DIR_ROOT' , trailingslashit( dirname( __FILE__ ) ) );

require_once 'classes/ACFObjects.class.php';

//bind the register hook
register_activation_hook( __FILE__ , array( 'ACFObjects' , 'on_activate' ) );

//bind the init
add_action( 'init' , array( 'ACFObjects' , 'on_init' ) );